//Alex Hepp
//ahepp1
#ifndef DIAGRAPHANALYZER_H
#define DIAGRAPHANALYZER_H

#include <vector>
#include <map>
#include <string>


void outStream(std::map<std::string, std::vector<std::string>>& dpair);

void writeOut(std::map<std::string, std::vector<std::string>>& dpair, char type);

std::map<std::string, std::vector<std::string>> findDigraphs(std::ifstream& input, std::vector<std::string>& dlist);

std::vector<std::string> getDigraph(std::ifstream& input);

#endif
