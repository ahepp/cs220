//
//
//
#include <stdio.h>
#include "funs.h"


int main() {
  float y = 0;
  float x = 0;

  printf("Input 2 numbers to add\n");
  scanf(" %f%f", &x, &y);
  float add = addition(x,y);

  printf("the sum of %.2f and %.2f is %.2f\n", x, y, add);
  return 0;
}
