#include <iostream>
#include <vector>
using std::cout; using std::endl; using std::vector;

void sum(vector<int>& v, int& result) {
  for(std::vector<int>::iterator it = v.begin(); it != v.end(); ++it) {
    result = *it + result;
  }
}

int main() {
vector<int> v;
for (int i = 0; i < 87; i = i + 2) {
v.push_back(i);
}
int result = 0;
sum (v, result);
cout << "sum of all v’s elements is: " << result << endl;
}
