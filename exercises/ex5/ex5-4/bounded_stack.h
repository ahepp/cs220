
#ifndef _BOUNDED_STACK_H
#define _BOUNDED_STACK_H

#include <iostream>

class BoundedStack : public Stack {
private:
  int limit;
public:
  BoundedStack(int cap, int lim): Stack(cap), limit(lim) {}
  BoundedStack(int lim): Stack(), limit(lim) {}
  BoundedStack(): Stack(), limit(1) {}

  void push(double x) override {
    if(size + 1 >= limit) {
      throw std::overflow_error("Stack upper limit reached.");
    }
    Stack::push(x);
  }


};

#endif
