//Alex Hepp
//ahepp1
#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main() {
  float num1 = 0;
  float num2 = 0;
  char operation = '0';
  float temp = 0;
  //int bool = 1;
  //int count1 = 0;
  //int count2 = 0;


  printf("Please enter an arithmetic expression using * and / only: \n");

  //takes in the first number of input characters
  if(scanf("%f", &num1) == 1) {
  temp = num1;
  }
  else {
    printf("malformed expression\n");
    return 1;
  }
  //continues reading the input after the first values
  for(;;) {
    int iter = scanf(" %c%f", &operation, &num2);
    if (iter == EOF) break;
    if (iter == 2) {
      if(operation == '*' || operation == '/') {
      num1 = temp;
      //checks for division by 0
        if(operation == '/' && num2 == 0) {
          printf("division by 0\n");
          return 2;
        }
        //does the multiplication if valid
        else if(operation == '*') {
          temp = num1 * num2;
          //count2++;
        }
        //does the divison if valid
        else if(operation == '/') {
          temp = num1 / num2;
          //count2++;
        }
        else {
          printf("malformed expression\n");
          return 1;
        }
      }
      else {
        printf("malformed expression\n");
        return 1;
      }
    }
    if (iter == 1 || iter == 0) {
      printf("malformed expression\n");
      return 1;
    }
  }
  //prints out the result of the entered expression, if valid.
  printf("%f\n",temp);
  return 0;
}
