// test_search_functions.c
// <STUDENT: ADD YOUR INFO HERE: name, JHED, etc.>
//Alex Hepp
//ahepp1


#include <stdio.h>
#include <assert.h>
#include "search_functions.h"


/*
 * Declarations for tester functions whose definitions appear below.
 * (You will need to fill in the function definition details, at the
 * end of this file, and add comments to each one.)
 * Additionally, for each helper function you elect to add to the
 * provided search_functions.h, you will need to supply a corresponding
 * tester function in this file.  Add a declaration for it here, its
 * definition below, and a call to it where indicated in main.
 */
void test_file_eq();      // This one is already fully defined below.
void test_populate_grid();
void test_find_right();
void test_find_left();
void test_find_down();
void test_find_up();
void test_find_all();


/*
 * Main method which calls all test functions.
 */
int main() {
  printf("Testing file_eq...\n");
  test_file_eq();
  printf("Passed file_eq test.\n\n");

  printf("Running search_functions tests...\n");
  test_populate_grid();
  test_find_right();
  test_find_left();
  test_find_down();
  test_find_up();
  test_find_all();

  /* You may add calls to additional test functions here. */

  printf("Passed search_functions tests!!!\n");
}



/*
 * Test file_eq on same file, files with same contents, files with
 * different contents and a file that doesn't exist.
 * Relies on files test1.txt, test2.txt, test3.txt being present.
 */
void test_file_eq() {
  FILE* fptr = fopen("test1.txt", "w");
  fprintf(fptr, "this\nis\na test\n");
  fclose(fptr);

  fptr = fopen("test2.txt", "w");
  fprintf(fptr, "this\nis\na different test\n");
  fclose(fptr);

  fptr = fopen("test3.txt", "w");
  fprintf(fptr, "this\nis\na test\n");
  fclose(fptr);

  assert( file_eq("test1.txt", "test1.txt"));
  assert( file_eq("test2.txt", "test2.txt"));
  assert(!file_eq("test2.txt", "test1.txt"));
  assert(!file_eq("test1.txt", "test2.txt"));
  assert( file_eq("test3.txt", "test3.txt"));
  assert( file_eq("test1.txt", "test3.txt"));
  assert( file_eq("test3.txt", "test1.txt"));
  assert(!file_eq("test2.txt", "test3.txt"));
  assert(!file_eq("test3.txt", "test2.txt"));
  assert(!file_eq("", ""));  // can't open file
}



void test_populate_grid(){

  char grid[MAX_SIZE][MAX_SIZE];
  int dims = populate_grid(grid, "grid.txt");
  assert(populate_grid(grid, "test1.txt") == -2);
  assert(populate_grid(grid, "test2.txt") == -2);
  assert(populate_grid(grid, "test3.txt") == -2);
  assert(populate_grid(grid, "nope") == -1);
  assert(file_eq("grid.txt", "grid.txt"));
  assert(populate_grid(grid, "grid.txt") == dims);
  assert( file_eq("grid.txt", "popgrid.txt"));

}


void test_find_right(){

  char grid[MAX_SIZE][MAX_SIZE];
  int dims = populate_grid(grid, "grid.txt");
  assert(find_right(grid, dims, "nope", stdout) == 0);
  assert(find_right(grid, dims, "top", stdout) == 1);
  assert(find_right(grid, dims, "pit", stdout) == 1);
  assert(find_right(grid, dims, "key", stdout) == 1);

}


void test_find_left(){

  char grid[MAX_SIZE][MAX_SIZE];
  int dims = populate_grid(grid, "grid.txt");
  assert(find_left(grid, dims, "tip", stdout) == 1);
  assert(find_left(grid, dims, "pot", stdout) == 1);
  assert(find_left(grid, dims, "nope", stdout) == 0);
  assert(find_left(grid, dims, "peet", stdout) == 0);

}


void test_find_down(){

  char grid[MAX_SIZE][MAX_SIZE];
  int dims = populate_grid(grid, "grid.txt");
  assert(find_down(grid, dims, "pop", stdout) == 1);
  assert(find_down(grid, dims, "pep", stdout) == 1);
  assert(find_down(grid, dims, "nope", stdout) == 0);
  assert(find_down(grid, dims, "key", stdout) == 1);

}


void test_find_up(){

  char grid[MAX_SIZE][MAX_SIZE];
  int dims = populate_grid(grid, "grid.txt");
  assert(find_up(grid, dims, "eye", stdout) == 1);
  assert(find_up(grid, dims, "pop", stdout) == 1);
  assert(find_up(grid, dims, "nope", stdout) == 0);
  assert(find_up(grid, dims, "pep", stdout) == 1);

}


void test_find_all(){

  char grid[MAX_SIZE][MAX_SIZE];
  int dims = populate_grid(grid, "grid.txt");
  assert(find_all(grid, dims, "pop", stdout) == 2);
  assert(find_all(grid, dims, "tip", stdout) == 1);
  assert(find_all(grid, dims, "nope", stdout) == 0);
  assert(find_all(grid, dims, "key", stdout) == 2);


}
