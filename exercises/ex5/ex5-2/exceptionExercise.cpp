#include <iostream>
#include <fstream>
#include <vector>
#include <exception>
#include <stdexcept>

/* Collect integers from a file; store them in a vector */
std::vector<int> readFile(char* filename) {
  std::ifstream fin(filename);
  if(!fin.is_open()) {
    throw std::ios_base::failure("File could not be opened.\n");
  }
  std::vector<int> numbers(10);

  int n = 0;
  size_t index = 0;
  while(true) {
    try{
      fin >> n;
      if (fin.fail() && !fin.eof()) { // if we failed to read an int, throw an exception
        throw std::invalid_argument("File contains non-integer data!\n");
      }
    }
    catch(std::invalid_argument& ex) {
      std::cout << ex.what() << std::endl;
      return numbers;
    }
    if (fin.eof()) { // if we're out of file, return
      return numbers;
    }

    // otherwise, just add it into the vector
    try{
      numbers.at(index) = n;
      index++;
    }
    catch(const std::out_of_range& e) {
      //std::cout << "Too many numbers in the file!" << std::endl
      //  << e.what() << std::endl;
      std::cout << "Resizing vector." << std::endl;
      numbers.resize(index + 1);
      numbers.at(index) = n;
      index++;
    }
  }

  throw std::logic_error("ERROR: should never get here!");
  return numbers;
}


int main(int argc, char **argv) {
  if (argc < 2) {
    std::cout << "Error: program requires a filename as an argument\n";
    return 0;
  }

  std::vector<int> numbers;
  try{
    numbers = readFile(argv[1]);
  }
  catch(const std::ios_base::failure& e) {
    std::cout << e.what() << std::endl;
  }

  std::cout << "Read numbers: ";
  for(int &i : numbers) {
    std::cout << i << " ";
  }
  std::cout << "\n";

  return 0;
}
