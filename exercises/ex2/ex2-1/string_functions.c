
#include <stdio.h>
#include <string.h>
#include "string_functions.h"

int concat(const char word1[], const char word2[], char result[], int result_capacity){
   int count = 0;
   int length = strlen(word1) + strlen(word2);
   int len1 = strlen(word1);
   int len2 = strlen(word2);
   printf("this is my message..");
   //TODO: replace the following stub with a correct function body so that
   //      this function behaves as indicated in the comment above
   //
   if (length < result_capacity) {
     for(int i = 0; i < len1; i++) {
       result[i] = word1[i];
       count++;
     }
    for(int i = count; i < count + len2 + 1; i++) {
      result[i] = word2[i - count];
    }
    return 0;
  }
  else{
    return 1;
  }
   //NOTE: you may not use the strcat or strcpy library functions in your solution!
   return -1;

}
