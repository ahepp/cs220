//Alex Hepp
//ahepp1

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <vector>
#include <regex>
#include <map>
#include <iterator>
#include "digraphAnalyzer.h"

using std::stringstream; using std::cout; using std::map;
using std::cin; using std::string; using std::endl;
using std::ifstream; using std::vector; using std::getline;

/*
* Function to create a string vector with the digraphs listed at the
* top of the input file. Takes in an open filestream and returns a vector with
* all digraphs.
*/
vector<string> getDigraph(ifstream& input) {
  string digraphs;
  string word;
  vector<string> dlist;
  int dcount = 0;
  input >> dcount;
  //Get the leading number out of the way, don't need to store this because
  //vectors have a .size() method.
  getline(input, digraphs);
  stringstream ss(digraphs);
  //Split up the digraph by witespace to store them all separately
  while(ss >> word) {
    dlist.push_back(word);
  }
  return dlist;
}

/*
* Function to go through the text in the provided input file and search for
* the digraphs in the inputted vector of digraphs, dlist. Takes in the open
* filesteam and the vector of digraphs returned from getDigraph and returns
* an ACSII sorted map with digraphs as keys and a vector of stings as
* cooresponding values.
*/
map<string, vector<string>> findDigraphs(ifstream& input, vector<string>& dlist) {
  string text;
  vector<string> temp;
  map<string, vector<string>> dpair;
  //Skip the first line in the text with all the listed digraphs.
  getline(input, text);
  //Read the entire file into the string text in one go.
  text.assign((std::istreambuf_iterator<char>(input)),(std::istreambuf_iterator<char>()));
  //Go through all the digraphs in the provided list.
  for(vector<string>::iterator it = dlist.begin(); it != dlist.end(); ++it) {
    //Create a regular expression to match any word containing the digraph,
    //and return the entire word without any punctuation.
    string expression = "[^\\s]*" + *it + "([^\\s.,?!]*)";
    std::regex search(expression, std::regex_constants::icase);
    //Create an iterator to go through all of the text (so it doesn't stop at
    //the first match).
    std::sregex_iterator next(text.begin(), text.end(), search);
    std::sregex_iterator end;
    while (next != end) {
      std::smatch match = *next;
      string data = match.str();
      //One liner to turn the entire word to lowercase as requested in assignment.
      std::transform(data.begin(), data.end(), data.begin(), ::tolower);
      temp.push_back(data);
      next++;
    }
    //Insert the pair of digraph and vector into the ordered map. Map will sort
    //automatically.
    dpair.insert(std::pair<string, vector<string>>(*it, temp));
    temp.clear();
  }
  return dpair;
}

/*
* Function to print out the digraphs and found words in one of three ways depending
* on the parameter type. When given a map of digraphs and a vector of strings,
* will write to stdout the map in ASCII order, reverse ASCII order, or by relative size
* of the vectors in the map.
*/
void writeOut(map<string, vector<string>>& dpair, char type){
  //If reverse ASCII requested, just use reverse iterators on the ASCII sorted map.
  if(type == 'r') {
    for(map<string, vector<string>>::reverse_iterator it = dpair.rbegin(); it!=dpair.rend(); ++it) {
      cout << it->first << ": [";
      for(vector<string>::iterator vit = it->second.begin(); vit != it->second.end(); ++vit) {
        cout << *vit;
        //Check to see if a comma is needed (make sure not the last string in vector).
        if(std::next(vit, 1) != it->second.end()) {
          cout << ", ";
        }
      }
      cout << "]" << endl;
    }
    return;
  }
  //Map already ASCII sorted, just formts and outputs it.
  else if (type == 'a'){
    for(map<string, vector<string>>::iterator it = dpair.begin(); it!=dpair.end(); ++it) {
      cout << it->first << ": [";
      for(vector<string>::iterator vit = it->second.begin(); vit != it->second.end(); ++vit) {
        cout << *vit;
        if(std::next(vit, 1) != it->second.end()) {
          cout << ", ";
        }
      }
      cout << "]" << endl;
    }
    return;
  }
  //More difficult one as maps are hard to sort by value vector size.
  else if (type == 'c') {
    //Creates a multimap, as some digraphs could have the same counts.
    std::multimap<int, string> counts;
    //Go through the digraph map and make a new map matching each digraph to
    //the number of words it occurs in. This map automatically sorts to the smallest
    //number in the first index and the largest in the last.
    for(map<string, vector<string>>::reverse_iterator it = dpair.rbegin(); it!=dpair.rend(); ++it) {
      counts.insert(std::pair<int, string>((it->second).size(),it->first));
    }
    //Now going through the counts map, pass the value of the multimap to the keys
    //of the digraph map to access the vectors of strings in the desired order.
    //Must use a reverse iterator here because the multimap is bottom heavy and
    //we want to list from largest vector size to least. Ties are automatically
    //broken by ASCII by the built in sorting of the multimap.
    for(std::multimap<int, string>::reverse_iterator rit = counts.rbegin(); rit!=counts.rend(); ++rit) {
      cout << rit->second << ": [";
      for(vector<string>::iterator vit = dpair[rit->second].begin(); vit != dpair[rit->second].end(); ++vit) {
        cout << *vit;
        if(std::next(vit, 1) != dpair[rit->second].end()) {
          cout << ", ";
        }
      }
      cout << "]" << endl;
    }
    return;
  }
  //If not one of the handled cases, exit with a return value of 1 and a message.
  else{
    cout << "Please enter a valid argument. Valid arguments are 'r', 'a', or 'c'." << endl;
    exit(1);
  }
}

/*
* Function to handle all the user input and output after the text file has been
* read through. When given a map of digraphs and the cooresponding string matches
* will accept user input and output text as desired by the assignment.
*/
void outStream(map<string, vector<string>>& dpair) {
  string word;
  bool found = false;
  cout << "q?>";
  while(cin >> word) {
    found = false;
    std::transform(word.begin(), word.end(), word.begin(), ::tolower);
    //End the while loop and the program when "quit" is inputted.
    if(word == "quit") {
      return;
    }
    //Clever one liner to check if the entire entered string is not empty and is
    //an integer.
    else if(!word.empty() && std::all_of(word.begin(), word.end(), ::isdigit)) {
      std::multimap<int, string> counts;
      //Turn the string object into an integer.
      char tempword[50];
      strcpy(tempword, word.c_str());
      //Check to see if the entered number is a frequency in the map.
      for(map<string, vector<string>>::reverse_iterator it = dpair.rbegin(); it!=dpair.rend(); ++it) {
        if((int)(it->second).size() == atoi(tempword)){
          found = true;
        }
      }
      //Go through and output all the digraphs with the entered frequency.
      for(map<string, vector<string>>::iterator it = dpair.begin(); it!=dpair.end(); ++it) {
        if(found == false) {
          cout << "None" << endl;
          break;
        }
        else if((int)(it->second).size() == atoi(tempword)){
          counts.insert(std::pair<int, string>((it->second).size(),it->first));
          for(std::multimap<int, string>::reverse_iterator rit = counts.rbegin(); rit!=counts.rend(); ++rit) {
            cout << rit->second << endl;

            for(vector<string>::iterator vit = dpair[rit->second].begin(); vit != dpair[rit->second].end(); ++vit) {
                cout << *vit << endl;
            }
          }
          counts.clear();
        }
      }
    }
    //If input is not a number, check to see if input is in the digraph list.
    else{
      for(map<string, vector<string>>::iterator it = dpair.begin(); it!=dpair.end(); ++it) {
        if(it->first == word) {
          found = true;
        }
      }
      for(map<string, vector<string>>::iterator it = dpair.begin(); it!=dpair.end(); ++it) {
        if(found == false) {
          cout << "No such digraph" << endl;
          break;
        }
        //If there is a match, output all the words in the keypair matching the digraph.
        else if(it->first == word) {
          cout << (it->second).size() << endl;
          for(vector<string>::iterator vit = it->second.begin(); vit != it->second.end(); ++vit) {
            cout << *vit << endl;
          }
        }
      }
    }
    cout << "q?>";
  }
}

/*
* Main function to take in command line arguments and call all the
* helper functions. Does some error checking to make sure input is correctly
* formatted. Must supply an input textfile and a character indicating sorting type.
*/
int main(int argsc, char * argsv[]) {

  if(argsc != 3) {
    cout << "Please enter an input file name and an argument (r, a, or c)." << endl;
    return 1;
  }

  ifstream input(argsv[1]);
  if(!input.is_open()) {
    cout << "File failed to open, please supply a valid text file." << endl;
    return 1;
  }

  vector<string> dlist = getDigraph(input);
  map<string, vector<string>> dpair = findDigraphs(input, dlist);
  writeOut(dpair, *argsv[2]);
  outStream(dpair);
  return 0;
}
