//live_addition.c

#include <stdio.h>

int main() {
  int first;
  int second;
  printf("Input first integer.\n");
  scanf("%d", &first);
  printf("Input second Integer.\n");
  scanf("%d", &second);
  printf("%d plus %d equals %d\n", first, second, first + second);
  return 0;
}
