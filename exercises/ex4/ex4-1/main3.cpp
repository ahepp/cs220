/**
 * Hint: resolve the TODOs in grade_list.h first.
 *
 * TODO: Write a program that declares a GradeList
 *       variable and adds to it all the even
 *       numbers 0-100:
 *
 *       {0, 2, 4, ..., 98, 100}
 *
 *       then prints out the minimum, maximum,
 *       median, mean and 75th percentile, all
 *       nicely labelled.
 */
 #include "grade_list.h"
 #include <iostream>
 #include <cassert>

 using std::cout;
 using std::endl;

int main() {
  GradeList gl;
  for(double i = 0; i <= 100; i = i + 2) {
    gl.add(i);
  }
  cout << "Mean is: " << gl.mean() << endl;
  cout << "Median is: " << gl.median() << endl;
  cout << "75th percentile is: " << gl.percentile(75.0) << endl;
}
