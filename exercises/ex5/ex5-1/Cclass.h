#ifndef _CCLASS_H
#define _CCLASS_H

#include <iostream>
#include <string>
#include "Aclass.h"

class C : public A {

private:
  int e;

public:
  C(int val = 0): e(val) { };  // automatically sets a & d to 0 w/ A()
  C(int eval, int aval, double dval): A(aval, dval), e(eval) {};
  void sete(int val) { e = val; };
  virtual std::string toString() const override {
    std::cout << "[Cclass: a = " << geta() << ", e = " << e << ", d = " << d << ", size = " << sizeof(*this) << "]" << std::endl;
    return " ";};
  int fun() const {
    return geta() * e * d;
  };
};


#endif
