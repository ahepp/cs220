//Alex Hepp
//ahepp1
#include "Tree.h"
#include <cstdlib>
#include <string>
#include <iostream>
#include <sstream>

using std::string; using std::cout; using std::endl; using std::stringstream;

//Constructor, when given an object, creates a new root with all pointer set to nullptr.
template<typename T>
Tree<T>::Tree(T ch): data(ch), kids(nullptr), sibs(nullptr), prev(nullptr) {}

//Destructor, recursively removes and frees all nodes in a tree through depth-
//first traversal.
template<typename T>
Tree<T>::~Tree() {
  if(kids) {
   delete kids;
  }
  if(sibs){
   delete sibs;
 }
}

// Caret operator, does the same thing as addChild. Rturns the current tree.
template<typename T>
 Tree<T>& Tree<T>::operator^(Tree<T>& rt) {
   addChild(&rt);
   return *this;
 }

 //Helper function to go through two trees using depth-first traversal and check if all
 //nodes are equal
template<typename T>
bool Tree<T>::isEqual(Tree<T> *cur,const Tree<T> *root) {
  if(cur->data != root->data) {
    return false;
  }
  if(cur->kids && root->kids) {
    return isEqual(cur->kids, root->kids);
  }
  if(cur->sibs && root->sibs) {
    return isEqual(cur->sibs, root->sibs);
  }
  //Checks if one tree has more sibs/kids than another.
  else if((cur->kids || root->kids) || (cur->sibs || root->sibs)) {
    return false;
  }
 return true;
}

//Overloaded == operator, just calls the isEqual helper function and returns its output.
template<typename T>
bool Tree<T>::operator==(const Tree<T> &root) {
  return isEqual(this, &root);
 }

//function to add a child to an existing tree.
template<typename T>
bool Tree<T>::addChild(T ch) {
  //checks if the root has any kids, if not, adds it as a kid.
  if(!kids) {
    Tree<T>* new_leaf = new Tree<T>(ch);
    kids = new_leaf;
    new_leaf->prev = this;
    return true;
  }
    //Otherwise add it as a sibling of the existing kids.
  else{
    return kids->addSibling(ch);
  }
}

//Overloaded method to add an entire existing root to the current root.
template<typename T>
bool Tree<T>::addChild(Tree<T> *root) {
  //Make sure passed root is valid (doesn't have any prev or sibs).
  if(root->prev || root->sibs) {
    return false;
  }
  //If current root has no kids, add passed root as kid.
  else if(!kids) {
    kids = root;
    root->prev = this;
    return true;
  }
  //otherwise add as sibling.
  else{
    return kids->addSibling(root);

  }
}

//method to only be called from addChild, add a sibling to a child when passed a character.
template<typename T>
bool Tree<T>::addSibling(T ch) {
  //Check to make sure method is being called from an addChild function,
  //otherwise we might be trying to add a sibling to a root which is bad.
  if(!this->prev) {
    return false;
  }
  Tree<T>* cur = this;
  //Test to see if leaf is already a child/sibling
  while(cur) {
    if(cur->data == ch) {
      return false;
    }
    cur = cur->sibs;
  }
  //create a new leaf and call the overloaded addSibling function.
  Tree<T>* new_leaf = new Tree<T>(ch);
  return addSibling(new_leaf);
}

//Overloaded addSibling function, takes in an entire root and adds it as a sibling
//to a kid.
template<typename T>
bool Tree<T>::addSibling(Tree<T> *root) {
  Tree<T>* cur = this;
  //Test to see if leaf is already a child/sibling
  while(cur) {
    if(cur->data == root->data) {
      return false;
    }
    cur = cur->sibs;
  }

  cur = this;
  //check to see if sibling should be ordered first, if so inserts new node and
  //shifts everything else to the right.
  if(cur->data > root->data) {
    root->sibs = cur;
    this->prev->kids = root;
    root->prev = this->prev;
    cur->prev = root;
    return true;
  }
  //go along the siblings until the correct place to add the new root is found
  while (cur->sibs && (cur->sibs->data < root->data)) {
    cur = cur->sibs;
  }
  //add the new root
  root->sibs = cur->sibs;
  cur->sibs = root;
  root->prev = cur;
  return true;
}

//Recursive function to convert a tree into a string, returns a string with
//every object in the tree separated by a new line (depth first traversal).
template<typename T>
string Tree<T>::toString() {
  stringstream s;
  s << data << '\n';
    if(kids) {
      s << kids->toString();
     }
    if(sibs){
      s << sibs->toString();
     }
  return s.str();
}

//Method to overload the << operator, just calls toString and outputs it as a string.
//returns os to be able to chain.
template<typename U>
std::ostream& operator<<(std::ostream& os, Tree<U>& rt) {
  os << rt.toString();
  return os;
}
