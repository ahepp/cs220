//Alex Hepp
//ahepp1
#include "CTree.h"
#include <cstdlib>
#include <string>
#include <iostream>

using std::string; using std::cout; using std::endl;

//Constructor, when given a character, creates a new root with all pointer set to nullptr.
CTree::CTree(char ch): data(ch), kids(nullptr), sibs(nullptr), prev(nullptr) {}

//Destructor, recursively removes and frees all nodes in a tree through depth-
//first traversal.
CTree::~CTree() {
  if(kids) {
   delete kids;
  }
  if(sibs){
   delete sibs;
 }
}

// Caret operator, does the same thing as addChild. Rturns the current tree.
 CTree& CTree::operator^(CTree& rt) {
   addChild(&rt);
   return *this;
 }

//Helper function to go through two trees using depth-first traversal and check if all
//nodes are equal
bool CTree::isEqual(CTree *cur,const CTree *root) {
  if(cur->data != root->data) {
    return false;
  }
  if(cur->kids && root->kids) {
    return isEqual(cur->kids, root->kids);
  }
  if(cur->sibs && root->sibs) {
    return isEqual(cur->sibs, root->sibs);
  }
  //Checks if one tree has more sibs/kids than another.
  else if((cur->kids || root->kids) || (cur->sibs || root->sibs)) {
    return false;
  }
 return true;
}

//Overloaded == operator, just calls the isEqual helper function and returns its output.
bool CTree::operator==(const CTree &root) {
   return isEqual(this, &root);
 }

//function to add a child to an existing tree.
bool CTree::addChild(char ch) {
  //checks if the root has any kids, if not, adds it as a kid.
  if(!kids) {
    CTree* new_leaf = new CTree(ch);
    kids = new_leaf;
    new_leaf->prev = this;
    return true;
  }
  //Otherwise add it as a sibling of the existing kids.
  else{
    return kids->addSibling(ch);
  }
}

//Overloaded method to add an entire existing root to the current root.
bool CTree::addChild(CTree *root) {
  //Make sure passed root is valid (doesn't have any prev or sibs).
  if(root->prev || root->sibs) {
    return false;
  }
  //If current root has no kids, add passed root as kid.
  else if(!kids) {
    kids = root;
    root->prev = this;
    return true;
  }
  //otherwise add as sibling.
  else{
    return kids->addSibling(root);

  }
}

//method to only be called from addChild, add a sibling to a child when passed a character.
bool CTree::addSibling(char ch) {
  //Check to make sure method is being called from an addChild function,
  //otherwise we might be trying to add a sibling to a root which is bad.
  if(!this->prev) {
    return false;
  }
  CTree* cur = this;
  //Test to see if leaf is already a child/sibling
  while(cur) {
    if(cur->data == ch) {
      return false;
    }
    cur = cur->sibs;
  }
  //create a new leaf and call the overloaded addSibling function.
  CTree* new_leaf = new CTree(ch);
  return addSibling(new_leaf);
}

//Overloaded addSibling function, takes in an entire root and adds it as a sibling
//to a kid.
bool CTree::addSibling(CTree *root) {
  CTree* cur = this;
  //Test to see if leaf is already a child/sibling
  while(cur) {
    if(cur->data == root->data) {
      return false;
    }
    cur = cur->sibs;
  }

  cur = this;
  //check to see if sibling should be ordered first, if so inserts new node and
  //shifts everything else to the right.
  if(cur->data > root->data) {
    root->sibs = cur;
    this->prev->kids = root;
    root->prev = this->prev;
    cur->prev = root;
    return true;
  }
  //go along the siblings until the correct place to add the new root is found
  while (cur->sibs && (cur->sibs->data < root->data)) {
    cur = cur->sibs;
  }
  //add the new root
  root->sibs = cur->sibs;
  cur->sibs = root;
  root->prev = cur;
  return true;
}

//Recursive function to convert a tree into a string, returns a string with
//every character in the tree separated by a new line (depth first traversal).
string CTree::toString() {
  string s;
  s.push_back(data);
  s.push_back('\n');
    if(kids) {
      s += kids->toString();
     }
    if(sibs){
      s += sibs->toString();
     }
  return s;
}

//Method to overload the << operator, just calls toString and outputs it as a string.
//returns os to be able to chain.
std::ostream& operator<<(std::ostream& os, CTree& rt) {
  os << rt.toString();
  return os;
}
