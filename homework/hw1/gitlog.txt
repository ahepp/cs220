commit 1373b58f94120a19d8d8b2942c5fd65bec073b87
Author: Alex Hepp <ahepp1999@gmail.com>
Date:   Mon Jun 3 17:14:33 2019 -0400

    made function work for all malformed expressions

commit 4567b06fdb1597ab3a7d2ad968f0c14344bdb546
Author: Alex Hepp <ahepp1999@gmail.com>
Date:   Mon Jun 3 16:23:34 2019 -0400

    more bugfixing, cleaned up code a little and made more inputs work.

commit 6e2dd2c7a2fb71801e34d24ee27a300cc967b556
Author: Alex Hepp <ahepp1999@gmail.com>
Date:   Mon Jun 3 10:30:05 2019 -0400

    More bugfixing, program now outputs correctly and can take _most_ inputs correctly

commit bda71e7497b08113fcb57c11819b582e0ce8acc0
Author: Alex Hepp <ahepp1999@gmail.com>
Date:   Mon Jun 3 09:39:58 2019 -0400

    Did some bugfixing, now should return helpful input based error messgaes.

commit 28604aa2d65801184f4d1db58e02532f37eeb242
Author: Alex Hepp <ahepp1999@gmail.com>
Date:   Mon Jun 3 09:20:05 2019 -0400

    First draft of hw1, should take in input and chck if it is in the correct form

commit 1580687539a98404be64feb566dee7677894a62a
Author: Alex Hepp <ahepp1999@gmail.com>
Date:   Thu May 30 11:52:04 2019 -0400

    completed exercise 1-3

commit 4181ea55d6ec3045019f8e54ec8fc4c3bc57b577
Author: Alex Hepp <ahepp1999@gmail.com>
Date:   Thu May 30 10:27:37 2019 -0400

    Added exersice 1-3

commit 9dcec03df3e7de8211ed4db4b61ceaf60cf0d166
Author: Alex Hepp <ahepp1999@gmail.com>
Date:   Thu May 30 10:07:44 2019 -0400

    added directory for exercises

commit 3c0a3810461102e6a4fbc36a98d12813a923befb
Author: Alex Hepp <ahepp1999@gmail.com>
Date:   Thu May 30 09:42:20 2019 -0400

    created README

commit fc7c32c4c487de46d6814a83c8e7005dc53fe275
Author: Alex Hepp <ahepp1999@gmail.com>
Date:   Thu May 30 09:38:34 2019 -0400

    updated README

commit 7b52f483c2c930e8c53faab42c56e55fc74e46ce
Author: Alex Hepp <ahepp1999@gmail.com>
Date:   Thu May 30 09:32:01 2019 -0400

    added a readme, deleted prefab readme file

commit 070946ae226bb9d2c6987217d27559b16c2e60af
Author: Alex Hepp <ahepp1999@gmail.com>
Date:   Tue May 28 15:43:00 2019 +0000

    Initial commit
