// word_search.c
// <STUDENT: ADD YOUR INFO HERE: name, JHED, etc.>
//Alex Hepp
//ahepp1


#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "search_functions.h"
/*
 * This is the HW3 main function that performs a word search.
 */
int main(int argc, char* argv[]) {
  //See if a filename is supplied, returns 1 if not.
  if(argc != 2) {
    printf("Please enter a command line argument.\n");
    return 1;
  }
  char popgrid[MAX_SIZE][MAX_SIZE];
  int dims = populate_grid(popgrid, argv[1]);
  char word[MAX_SIZE + 1];
  int valid =0;
  int scan = scanf("%s", word);
  //scan for words to serch for until Ctr+D is read.
  while(scan != EOF) {
    for(int i = 0; word[i]; i++) {
      //makes sure the inputted word is really a word.
      if(isalpha(word[i]) && ((int)strlen(word) <= dims)) {
        word[i] = tolower(word[i]);
        valid = 1;
      }
      else{
        printf("Please try again with a real word.\n");
      }
     }
   if(valid) {
     find_all(popgrid, dims, word, stdout);
   }
   scan = scanf("%s", word);
  }
  return 0;
}
