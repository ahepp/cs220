
#include <iostream>
#include <sstream>
#include <string>

using std::stringstream; using std::cout;
using std::cin; using std::string; using std::endl;

void findWords(string str, int k) {

  string word;
  stringstream ss(str);
  while(ss >> word) {
    if((int)word.length() > k) {
      cout << word << endl;
    }

  }
}


int main() {
  string str = "Here is a bunch of space separated words.";
  int k = 3;
  findWords(str,k);
  return 0;
}
