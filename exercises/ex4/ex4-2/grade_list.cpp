#include "grade_list.h"
#include <cassert>

// You will add function definitions to this file.
// See TODOs in grade_list.h.



//////////// things to be added for part 2  /////////////////

// TODO: write a constructor (in grade_list.cpp) that has a
// parameter indicating the starting length of the array.
    GradeList::GradeList() : gradeArr(new double[1]), capacity(1), count(0) { }

    GradeList::GradeList(int capacity)  : gradeArr(new double[capacity]), capacity(capacity), count(0) { }

    double* GradeList::begin() {
      return &gradeArr[0];
    }

    double* GradeList::end() {
      return &gradeArr[count];
    }

    void GradeList::resize() {
      double* newGrades = new double[capacity * 2];
      for (int i = 0; i < capacity; i++) {
        newGrades[i] = gradeArr[i];
      }
      capacity *= 2;
      delete[] gradeArr;
      gradeArr = newGrades;
    }
    
    void GradeList::add(int howmany, double * grades) {
      for(int i = 0; i < howmany; i++) {
        if(count + 1 > capacity) {
          resize();
        }
      gradeArr[count] = grades[i];
      count++;
      }
    }

    void GradeList::add(double grade) {
      add(1, &grade);
    }

    void GradeList::clear() {
      capacity = 1;
      double* newGrades = new double[capacity];
      delete[] gradeArr;
      gradeArr = newGrades;
      count = 0;

    }

    double GradeList::min() {
      assert(count > 0);   // no values otherwise
      double minval = gradeArr[0];
      for (int i = 1; i < count; i++) {
        if (gradeArr[i] < minval)
          minval = gradeArr[i];
      }
      return minval;
    }


// TODO: write a function (in grade_list.cpp) to clear the list
// of all values, making the array as small as possible
