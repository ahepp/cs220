// 601.220 - example of inheritance, virtual functions, dynamic binding

#include "Aclass.h"
#include "Bclass.h"
#include "Cclass.h"
#include <iostream>


using std::cout;
using std::endl;

int main (void) {
  //A aobj(10);
  B bobj(20);
  C cobj(15);


  // Part 2
  //cout << aobj.toString() << endl;
  cout << bobj.toString() << endl;
  cout << cobj.toString() << endl;
  //aobj.seta(13);
  //aobj = bobj;
  //cout << aobj.toString() << endl;
  bobj.setb(27);
  //bobj = aobj;
  cout << bobj.toString() << endl;

  // Part 3
  //cout << aobj.fun();
  bobj.seta(1);
  bobj.setd(2);
  cout << bobj.fun() << endl;
  bobj.setb(2);
  cout << bobj.fun() << endl;


  cobj.seta(5);
  cobj.setd(3);
  cout << cobj.fun() << endl;
  cobj.sete(16);
  cout << cobj.fun() << endl;

  return 0;

}
