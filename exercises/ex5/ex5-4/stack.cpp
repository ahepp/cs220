#include <stdexcept>
#include <exception>
#include <iostream>
#include <cstring>
#include "stack.h"

using std::istream;  using std::ostream;

double Stack::pop() {
  if(isEmpty()) {
    throw std::underflow_error("Stack is empty!");
  }
  double pop = data[size];
  data[size] = 0;
  size--;
  return pop;
}

double Stack::top() const {
  if(isEmpty()) {
    throw std::underflow_error("Stack is empty!");
  }
  return data[size];
}

void Stack::push(double d) {
  if(++size >= cap) {
    double* temp = new double[cap * 2];
    if(!temp) {
      throw std::overflow_error("Stack increase failed.");
    }
    memcpy(temp, data, cap * sizeof(double));
    cap = cap * 2;
    delete[] data;
    data = temp;
  }
  data[size] = d;
}





// UNCOMMENT AND FILL IN DURING PART 3


std::ostream& operator<<(std::ostream& os, const Stack& s) {
  for(Stack::const_iterator it = s.cbegin(); it != s.cend(); ++it) {
    os << *it << " ";
  }
  return os;
}

std::istream& operator>>(std::istream& is, Stack& s) {
  double in;
  while(is >> in) {
    s.push(in);
  }
  return is;
}
