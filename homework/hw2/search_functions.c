// search_functions.c
// <STUDENT: ADD YOUR INFO HERE: name, JHED, etc.>
//Alex Hepp
//ahepp1


#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "search_functions.h"



/*
 * Given a filename and a MAX_SIZExMAX_SIZE grid to fill, this function
 * populates the grid and returns n, the actual grid dimension.
 * If filename_to_read_from can't be opened, this function returns -1.
 * If the file contains an invalid grid, this function returns -2.
 */
int populate_grid(char grid[][MAX_SIZE], char filename_to_read_from[]){

  //Opens the file and creates the gridfile variable.
  FILE* gridfile = fopen(filename_to_read_from,"r");
  //Opens a file to write into the result from populating the grid.
  FILE* output = fopen("popgrid.txt","w");
  //Tests to see if the given file could be opened.
  if(gridfile==NULL) {
    printf("Grid file failed to open.\n");
    return -1;
  }

  char rowvals[MAX_SIZE + 1];
  char temprow[MAX_SIZE + 1];
  int ncol = 0;
  int nrow = 0;
  //tests to make sure the grid is valid.
  int numCollected = fscanf(gridfile, "%s", rowvals);
  while(numCollected == 1) {
    ncol = 0;
    for(int n = 0; n < MAX_SIZE + 1; n++) {
      //Checks if every character in the string is alpha.
      if(isalpha(rowvals[n])) {
        //turns all letters into lowercase for ease of parsing.
        rowvals[n] = tolower(rowvals[n]);
        temprow[n] = rowvals[n];
        grid[nrow][n] = rowvals[n];
        ncol++;
      }
      else{
        //add the null characters to the end....yeah..
        rowvals[ncol + 1] = '\0';
        temprow[ncol + 1] = '\0';
        break;
      }
    }

    nrow++;
    numCollected = fscanf(gridfile, "%s", rowvals);
    //Make sure every line in the grid is of the same length.
    if(strlen(rowvals) != strlen(temprow)) {
      printf("Invalid Grid\n");
      return -2;
    }
  }
  ncol = strlen(temprow);
  //Makes sure the gris is at mose 10x10 (and not empty).
  if((ncol != nrow) || (ncol >= MAX_SIZE) || (ncol <= 0)) {
    printf("Invalid Grid\n");
    return -2;
  }
  //writes the grid to an output file to make sure the input coppied correctly.
  for(int i =0 ; i < nrow; i++) {
    for(int j = 0; j < ncol; j++) {
      fprintf(output, "%c", grid[i][j]);
    }
    fprintf(output, "\n");
  }
  fclose(gridfile);
  fclose(output);
  return nrow;
}


/*
 *Given a grid created in the populate grid function and its dimensions,
 *searches through it to find a given word. Once first letter is found
 *only searches to the right of that index.
 *Returns 0 if word was successfully found, and 1 if it was not.
 */
int find_right(char grid[][MAX_SIZE], int n, char word[], FILE *write_to){
  int found = 0;
  //searches through all indicies of the grid.
  for(int i =0 ; i < n; i++) {
    for(int j = 0; j < n; j++) {
      //tries to match character at index with first letter of word.
      if(grid[i][j] == word[0]) {
        //searches to the right of that index to see if next letter matches word
        for(int n = 1; n <= (int)strlen(word) - 1; n++) {
          if(grid[i][j + n] != word[n]) {
            break;
          }
          else if(n == (int)strlen(word) - 1) {
            fprintf(write_to, "%s %d %d %c\n",word, i, j, 'R');
            found++;
          }
        }
      }
    }
  }
  if(!found) {
    return 0;
  }
  return found;
}


/*
 *Given a grid created in the populate grid function and its dimensions,
 *searches through it to find a given word. Once first letter is found
 *only searches to the left of that index.
 *Returns 0 if word was successfully found, and 1 if it was not.
 */
int find_left (char grid[][MAX_SIZE], int n, char word[], FILE *write_to){
  int found = 0;
  //searches through all indicies of the grid.
  for(int i =0 ; i < n; i++) {
    for(int j = 0; j < n; j++) {
      //tries to match character at index with first letter of word.
      if(grid[i][j] == word[0]) {
        //searches to the right of that index to see if next letter matches word
        for(int n = 1; n <= (int)strlen(word) - 1; n++) {
          if(grid[i][j - n] != word[n]) {
            break;
          }
          else if(n == (int)strlen(word) - 1) {
            fprintf(write_to, "%s %d %d %c\n",word, i, j, 'L');
            found++;
          }
        }
      }
    }
  }
  if(!found) {
    return 0;
  }
  return found;
}


/*
 *Given a grid created in the populate grid function and its dimensions,
 *searches through it to find a given word. Once first letter is found
 *only searches below that index.
 *Returns 0 if word was successfully found, and 1 if it was not.
 */
int find_down (char grid[][MAX_SIZE], int n, char word[], FILE *write_to){
  int found = 0;
  //searches through all indicies of the grid.
  for(int i =0 ; i < n; i++) {
    for(int j = 0; j < n; j++) {
      //tries to match character at index with first letter of word.
      if(grid[i][j] == word[0]) {
        //searches to the right of that index to see if next letter matches word
        for(int n = 1; n <= (int)strlen(word) - 1; n++) {
          if(grid[i + n][j] != word[n]) {
            break;
          }
          else if(n == (int)strlen(word) - 1) {
            fprintf(write_to, "%s %d %d %c\n",word, i, j, 'D');
            found++;
          }
        }
      }
    }
  }
  if(!found) {
    return 0;
  }
  return found;

}


/*
 *Given a grid created in the populate grid function and its dimensions,
 *searches through it to find a given word. Once first letter is found
 *only searches above that index.
 *Returns 0 if word was successfully found, and 1 if it was not.
 */
int find_up (char grid[][MAX_SIZE], int n, char word[], FILE *write_to){
  int found = 0;
  //searches through all indicies of the grid.
  for(int i =0 ; i < n; i++) {
    for(int j = 0; j < n; j++) {
      //tries to match character at index with first letter of word.
      if(grid[i][j] == word[0]) {
        //searches to the right of that index to see if next letter matches word
        for(int n = 1; n <= (int)strlen(word) - 1; n++) {
          if(grid[i - n][j] != word[n]) {
            break;
          }
          else if(n == (int)strlen(word) - 1) {
            fprintf(write_to, "%s %d %d %c\n",word, i, j, 'U');
            found++;
          }
        }
      }
    }
  }
  if(!found) {
    return 0;
  }
  return found;
}


/*
 *Given a grid populated by populate gris function and its dimensions,
 *calls all directional search functions to find all instances of a word
 *in the grid. If the word is not found in any direction, returns 1.
 *If the word is found at least once, returns 0.
*/
int find_all  (char grid[][MAX_SIZE], int n, char word[], FILE *write_to) {
  int found = 0;
  //calls all functions and adds what they return.
  found = find_right(grid, n, word, write_to) + find_left(grid, n, word, write_to) + find_down(grid, n, word, write_to) + find_up(grid, n, word, write_to);
  //if all functions return 0 (word not found) sum will be 0,
  //so print word not found.
  if (found == 0) {
    fprintf(write_to, "%s - Not Found\n", word);
    return 0;
  }
return found;
}


/*
 * Reads lhs and rhs character by character until either reaches eof.
 * Returns true if the files can be read and the two files match
 * character by character. Returns false if two files either can't be
 * opened or don't match. The definition of this function is provided
 * for you.
 */
int file_eq(char lhs_name[], char rhs_name[]) {
  FILE* lhs = fopen(lhs_name, "r");
  FILE* rhs = fopen(rhs_name, "r");

  // don't compare if we can't open the files
  if (lhs == NULL || rhs == NULL) return 0;

  int match = 1;
  // read until both of the files are done or there is a mismatch
  while (!feof(lhs) || !feof(rhs)) {
	if (feof(lhs) ||                      // lhs done first
		feof(rhs) ||                  // rhs done first
		(fgetc(lhs) != fgetc(rhs))) { // chars don't match
	  match = 0;
	  break;
	}
  }
  fclose(lhs);
  fclose(rhs);
  return match;
}
